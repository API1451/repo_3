package mavenprog1;
import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
public class Test1 {
	static Prime obj1;

	@BeforeAll
	public static void abc1() {
		obj1 = new Prime();
	
		System.out.println("I am Before All");
	}
	@BeforeEach
	public void abc2() {
		
		System.out.println("I am Before Each");
	}
	@Test
	public void test1() {
		assertEquals(obj1.isPrime(10),false);
		System.out.println("TestCase1 satisfied");

	}
	@Test
	public void test2() {
		assertEquals(obj1.isPrime(11),true);
		System.out.println("TestCase2 satisfied");
	}
	@AfterEach
	public void abc5() {
		System.out.println("I am aftereach");
	}
	@AfterAll
	public static void abc6() {
		System.out.println("I am afterall");
	}
}